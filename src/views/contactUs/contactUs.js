import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination,
  CSelect,
  CButton
} from '@coreui/react'
import '../../globalVar'
import './contactUs.scss'



const ConatctUs = () => {
  const history = useHistory()

  
  const [data,setData]=useState([])
  const [refresh, setRefresh] = useState(false)
  const [errorMessage, setErrorMessage] = useState();
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPages, setTotalPages] = useState()
  const[succesAdd,setSuccessAdd]=useState()
  const[loading,setLoading]=useState('')
  const [pageStatus,setPageStatus]=useState(0)
  const tokenString = localStorage.getItem("token");
  const userToken = JSON.parse(tokenString);
  const [url,setUrl]=useState(`contactUs/viewAllContactUsRequests?`)

  const handleUrlFilter=(e)=>{
    setCurrentPage(1)
    if(e.target.value=='0'){setUrl(`contactUs/viewAllContactUsRequests?`)}
    else if(e.target.value=='active'){setUrl(`contactUs/viewAllContactUsRequests?seen=1&`)}
  
    else if(e.target.value=='notActive'){setUrl(`contactUs/viewAllContactUsRequests?seen=0&`)}
  
    else {setUrl(`contactUs/viewAllContactUsRequests?`)}
    
  }
  useEffect(async()=>{
    const fetchFAQs=async(e)=>{
  
  
    
    try {
      const responsee = await fetch(
        `${global.apiUrl}/${url}page=${currentPage}`,
        {
          method: "GET",
          headers: {
            Authorization: "Bearer " + userToken,
                       // "Content-Type": "application/json",
                      //'Access-Control-Allow-Origin': 'https://localhost:3000',
                      // 'Access-Control-Allow-Credentials': 'true',
            Accept: "application/json",
          },
  
      
        }
      );
      if(responsee.status==204){  setData([])
        setTotalPages()}
      const response = await responsee.json();
      // console.log('response',response);
      console.log('faqs',response);
    if(response.message== "Contact us requests retrieved successfully!"){
     setData(response.payload.data)
     setTotalPages(response.payload.last_page)
  
    }
      if(response.message&&response.message=="Unauthorized or invalid token!"){
      localStorage.removeItem("token");
      localStorage.clear()
   
    history.push("/login");
      }
     
    } catch (err) {
      console.log(err);
     
    }
  
    // setLoading(false)
    
    
    }
  
    fetchFAQs()
  },[currentPage,refresh,url])

  
const handleAcivate=async(id)=>{
  document.getElementById('root').style.opacity=0.75;
   try {
     const responsee = await fetch(
       `${global.apiUrl}/contactUs/setAsSeen?id=${id}`,
       {
         method: "GET",
         headers: {
           Authorization: "Bearer " + userToken,
 
           Accept: "application/json",
         },
 
     
       }
     );
     const response = await responsee.json();
     
     console.log(response);
 
     if(response.message){
      setRefresh(!refresh)
  document.getElementById('root').style.opacity=1;
    
      }
 
 
     if(response.message&&response.message=="Unauthorized or invalid token!"){
  document.getElementById('root').style.opacity=1;
 
     localStorage.removeItem("token");
     localStorage.clear()
  
   history.push("/login");
     }
    
   } catch (err) {
     console.log(err);
    
   }
   document.getElementById('root').style.opacity=1;
 
 }
 const [activeUser,setActiveUser]=useState('')
 const handleShow=(item)=>{
   setActiveUser(item)
   setPageStatus(1)
 }
 const handleBack=(item)=>{
  setActiveUser('')
  setPageStatus(0)
}
  return (
    
    <CRow>
      {pageStatus==0&&
       <CCol xl={12}>
       <CCard>
         <CCardHeader>
           <CCol md='12'><strong>Contact us Requests</strong></CCol>
           
           <CCol md="4" lg="4" xl="4" >
          
          <CSelect custom name="select"   onChange={(e)=>handleUrlFilter(e)}>
            <option value='0' >Filter (All Requestes) </option>
            <option value='active' >Seen</option>
        
             <option value='notActive' >Unseen</option>
           
         </CSelect>
       
 
       </CCol>
     
         </CCardHeader>
         <CCardBody className='usersTabel'>
         {data.length>0&&<CDataTable
           items={data}
           fields={['id','email','phone', 'message','created_at','seen','actions']}
           hover
           striped
           sorter
          
           // clickableRows
           // onRowClick={(item) => history.push(`/users/${item.id}`)}
           scopedSlots = {{
             'seen':
               (item)=>(
                 <td>
                   {item.seen==1?  <CBadge color='primary'>
                     Seen
                   </CBadge>
                   :
                   <CBadge color='dark'>
                 Not Seen
                 </CBadge>
                   }
        
                 </td>
               ),
               'created_at':
               (item)=>(
                 <td>
                {item.created_at.slice(0,10)}
                 </td>
               ),
               'actions':
               (item)=>(
                 <td>
                   {item.seen==1?<>
                    <CBadge className="p-1 m-1 " color="success" style={{cursor: 'not-allowed'}} >Seen</CBadge>
                                               
                                                     </>:
                                                     
                                                     <CBadge className="p-1 m-1 badg-click" color="primary" onClick={()=>handleAcivate(item.id)} >Set as Seen</CBadge>                       
                                                     
                                                     }
                 
                  
                 </td>
               ),

           }}
         />}
        {totalPages&&      <CPagination
           align="center"
           addListClass="some-class"
           activePage={currentPage}
           pages={totalPages}
           onActivePageChange={setCurrentPage}
           className='faqsPage'
         />}
         </CCardBody>
       </CCard>
     </CCol>
      }
    
    </CRow>
  )
}

export default ConatctUs
