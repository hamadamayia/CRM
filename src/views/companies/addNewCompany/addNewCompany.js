import React, { useState, useEffect } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CPagination,
  CDataTable,
  CSelect,
  CFormText,
  CTextarea,
  CFormGroup,
  CLabel,
  CSwitch,
  CInputFile,
  CLink,
  CFade,
  CCollapse,
  CBadge,
  CRow
} from '@coreui/react'
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import CIcon from '@coreui/icons-react'
import './addNewCompany.scss'

import { CAlert } from '@coreui/react'
import { useHistory } from "react-router-dom";
import { useTranslation } from 'react-i18next';

import '../../../globalVar'
// import { set } from 'core-js/core/dict'
// import MIN_SAFE_INTEGER from 'core-js/fn/number/min-safe-integer'
const AddNewCompany = () => {
  const [t, i18n] = useTranslation();
  let history = useHistory();
  const [visible, setVisible] = useState(10)

  const [fetchedData, setfetchedData] = useState([])
  const [refresh, setRefresh] = useState('')
  const [errorMessage, setErrorMessage] = useState();

  const [succesAdd, setSuccessAdd] = useState()
  const [loading, setLoading] = useState('')
  const tokenString = localStorage.getItem("token");
  const userToken = JSON.parse(tokenString);
  const [pickedImg, setPickedImg] = useState('')
  // const userId = localStorage.getItem("user_id");
  // const user_id = JSON.parse(userId);

  const [upData, setUpData] = useState({
    name_en: '',
    name_ar: '',
    criteriaDirection: '',
    criteriaDigits: '',
    countryId: '',
    departmentAr: '',
    departmentEn: '',
    userName: '',
    userUserName: '',
    email: '',
    extentionNumber: '',
    password: '',
    confirmPassword: '',
    userPosition: '',
    address:''


  })
  const { name_en,
    name_ar,
    criteriaDirection,
    criteriaDigits,
    countryId,
    departmentAr,
    departmentEn,
    userName,
    userUserName,
    email,
    extentionNumber,
    password,
    confirmPassword,
    userPosition,
    address


  } = upData;

  useEffect(async () => {
    const fetchCountries = async (e) => {
      try {
        const responsee = await fetch(
          `${global.apiUrl}/super/countries?paginate=0`,
          {
            method: "GET",
            headers: {
              Authorization: "Bearer " + userToken,

              Accept: "application/json",
            },
          }
        );
        const response = await responsee.json();
        console.log(response);

        if (response.success) {
          setfetchedData(response.payload)
        }


        if (response.message && response.message == "Unauthenticated.") {
          localStorage.removeItem("token");
          localStorage.clear()

          history.push("/login");
        }

      } catch (err) {
        console.log(err);

      }

      // setLoading(false)


    }

    fetchCountries()
  }, [refresh])



  const handleData = (e) => {
    setUpData({ ...upData, [e.target.name]: e.target.value })

    setErrorMessage('')
    setSuccessAdd('')
  }
  const [phones, setPhones] = useState([""])

  const handlePhones = (i) => (e) => {
    setPhones(
      ...[phones, (phones[i] = e.target.value)]
    );
    console.log('phones', phones)

  };

  // add row
  const [addRow, setaddrow] = useState([]);
  const handladdRow = (e) => {
    setaddrow([...addRow, 1]);
  };



  const handleAddCompany = async (e) => {
    e.preventDefault()
    setLoading(true)

    setErrorMessage('')
    setSuccessAdd('')

    const data = new FormData();
    name_en && data.append("company[name_en]", name_en);
    criteriaDirection && data.append("company[criteria_direction]", criteriaDirection);
    name_ar && data.append("company[name_ar]", name_ar);
    criteriaDigits && data.append("company[criteria_digits]", criteriaDigits);
    countryId && data.append("company[country_id]", countryId);
    address && data.append("company[address]", address);
    departmentAr && data.append("department[name_ar]", departmentAr);
    departmentEn && data.append("department[name_en]", departmentEn);
    userName && data.append("user[name]", userName);
    userUserName && data.append("user[username]", userUserName);
    email && data.append("user[email]", email);
    extentionNumber && data.append("user[extension_number]", extentionNumber);
    password && data.append("user[password]", password);
    confirmPassword && data.append("user[password_confirmation]", confirmPassword);
    userPosition && data.append("user[position]", userPosition);
    phones.map((item, index) => {
      phones[index] != '' && data.append("company[phones][]", phones[index]);
    });
    pickedImg && data.append('company[logo]', pickedImg);


    try {
      const responsee = await fetch(
        `${global.apiUrl}/super/companies`,
        {
          method: "POST",
          headers: {
            Authorization: "Bearer " + userToken,
            //  "Content-Type": "application/json",
            //'Access-Control-Allow-Origin': 'https://localhost:3000',
            // 'Access-Control-Allow-Credentials': 'true',
            Accept: "application/json",
          },
          body: data
          ,

        }
      );
      const response = await responsee.json();
      console.log('response', response);
      console.log(response);
      setVisible(10)
      if (response.success) {
        await setVisible(6)
        setSuccessAdd(i18n.language == 'ar' ? "تمت اضافة شركة بنجاح" : "New Company Added Successfuly")
document.getElementById('phone1').value=''

        setUpData({
          name_en: '',
          name_ar: '',
          criteriaDirection: '',
          criteriaDigits: '',
          countryId: '',
          departmentAr: '',
          departmentEn: '',
          userName: '',
          userUserName: '',
          email: '',
          extentionNumber: '',
          password: '',
          confirmPassword: '',
          userPosition: '',
          address:''
        })
        setPhones([""])
        setPickedImg('')
        setaddrow([])
        setVisible(6)
      }
      else {

        setVisible(10)
        setErrorMessage(response.messages)


      }


    } catch (err) {
      console.log(err);

    }

    setLoading(false)
  }





  const handleImg = (e) => {
    if (e.target.files[0]) { setPickedImg(e.target.files[0]) }
  }
  console.log('data', upData)
  return (
    <div className="c-app c-default-layout flex-row align-items-center register-cont">

      <CContainer>


        <CCard className="">



          <CCardHeader>
            <CRow className=" row-gap-15">

              <CCol md="6" lg="6" xl="6" className="justify-content-center align-self-center align-items-center place-items-center text-capitalize">
                <strong>{t('Add New Company')}</strong>
              </CCol>
            </CRow>
          </CCardHeader>

          <CRow>
            <CCol xs="12" sm="12" md="12" className=''>
              <CForm onSubmit={(e) => { handleAddCompany(e) }}>
                <CCardBody>
                  <CCard>
                    <CCardBody>
                      <CRow >
                        <CCol md='12'> <strong>
                          {i18n.language == 'ar' ? "معلومات الشركة :" : "Company Informations :"}
                          </strong></CCol>

                        {/* className="justify-content-center" */}
                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "الاسم الانكليزي" : "English Name"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="name_en"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? "الاسم الانكليزي" : "English Name"}
                                value={upData.name_en} />
                            </CCol>
                          </CFormGroup>

                        </CCol>

                        <CCol md="6" lg="6">
                          <CFormGroup row className='arabic-align'>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">الاسم العربي</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">


                              <CInput name="name_ar" required
                                onChange={handleData}

                                placeholder="الاسم العربي" value={upData.name_ar} />


                            </CCol>
                          </CFormGroup>

                        </CCol>

                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "اسم القسم بالانكليزية" : " Englsih Department Name"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="departmentEn"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? "اسم القسم بالانكليزية" : " Englsih Department Name"}
                                value={departmentEn} />
                            </CCol>
                          </CFormGroup>

                        </CCol>

                        <CCol md="6" lg="6">
                          <CFormGroup row className='arabic-align'>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">اسم القسم العربي</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CInput name="departmentAr" required
                                onChange={handleData}
                                placeholder="اسم القسم العربي" value={upData.departmentAr} />
                            </CCol>
                          </CFormGroup>

                        </CCol>

                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "اتجاه المعايير" : "Criteria Direction"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CSelect custom name="criteriaDirection" id="select"
                                required value={criteriaDirection} onChange={(e) => handleData(e)}>
                                <option value='' >{i18n.language == 'ar' ? "اختر اتجاه" : "Select direction"}
                                </option>

                                <option value='left'>{i18n.language == 'ar' ? "يسار" : "Left"}</option>
                                <option value='right'>{i18n.language == 'ar' ? "يمين" : "Right"}</option>

                              </CSelect>
                            </CCol>
                          </CFormGroup>

                        </CCol>
                        <CCol md="6" lg="6" xl="6">
                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">{i18n.language == 'ar' ? "أرقام المعايير" : "Criteria Digits"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CInput name="criteriaDigits"
                                required
                                type='number'
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? "أرقام المعايير" : "Criteria Digits"}
                                value={upData.criteriaDigits} />
                            </CCol>
                          </CFormGroup>
                        </CCol>
                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "الدولة" : "Country"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CSelect custom name="countryId"
                                required value={upData.countryId} onChange={(e) => handleData(e)}>
                                <option value='' >
                                  {i18n.language == 'ar' ? "اختر دولة" : "Select Country"}
                                </option>
                                {fetchedData.length > 0 && fetchedData.map((country) => {
                                  return (<option value={country.id} key={country.id}>
                                    {i18n.language == 'ar' ? country.name_ar : country.name_en}
                                  </option>)
                                })}

                              </CSelect>
                            </CCol>
                          </CFormGroup>

                        </CCol>
                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "العنوان" : "Address"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="address"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? "العنوان" : "Address"}
                                value={upData.address} />
                            </CCol>
                          </CFormGroup>

                       </CCol>
                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "هاتف (1)" : "Phone (1)"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="c"
                                required
                                id='phone1'
                                //  value={`${phones[0]}`}
                                onChange={handlePhones(0)}
                                placeholder={i18n.language == 'ar' ? "هاتف (1)" : "Phone (1)"} />
                            </CCol>
                          </CFormGroup>
                        </CCol>
                        {addRow.length > 0 &&
                          addRow.map((row, index) => {
                            return (
                              <CCol md="6" lg="6" xl="6" key={index}>

                                <CFormGroup row>
                                  <CCol md="12">
                                    <CLabel htmlFor="text-input">
                                      {i18n.language == 'ar' ? `هاتف (${(index + 2)})` : `Phone (${(index + 2)})`}</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="12">

                                    <CInput name="name_en"
                                      onChange={handlePhones(index + 1)}
                                      placeholder={i18n.language == 'ar' ? `هاتف (${(index + 2)})` : `Phone (${(index + 2)})`}
                                    />
                                  </CCol>
                                </CFormGroup>

                              </CCol>
                            );
                          })}

                        <CCol md="12" lg="12" xl="12">
                          <br></br>
                          <CButton className='col-md-2'
                            onClick={handladdRow}
                            color="info" block type='button'>
                            {i18n.language == 'ar' ? `إضافة هاتف آخر` : `Add More Phones`}
                          </CButton>
                          <br></br>
                        </CCol>
                        <CCol md='6' className='p-4'>
                          <CFormGroup row >
                            <CLabel col md={12}>{i18n.language == 'ar' ? `الشعار` : `Logo`}</CLabel>
                            <CCol xs="12" md="12">

                              <CInputFile required accept="image/*" custom id="custom-file-input" onChange={(e) => { handleImg(e) }} />

                              <CLabel htmlFor="custom-file-input" variant="custom-file">
                                {pickedImg ? pickedImg.name : i18n.language == 'ar' ? `اختر صورة ...` : `Choose image ...`}

                              </CLabel>
                            </CCol>
                          </CFormGroup>
                        </CCol>
                        <CCol md='6'>
                          {pickedImg ? <img className="imgLogo" src={URL.createObjectURL(pickedImg)}></img> :
                            null}
                        </CCol>
                        <CCol md='12'> <strong>{i18n.language == 'ar' ? `معلومات المستخدم :` : `User Informations :`}</strong></CCol>
                        <CCol md="6" lg="6" xl="6">
                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">{i18n.language == 'ar' ? `الاسم` : `Name`}</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="userName"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? `الاسم` : `Name`}
                                value={upData.userName} />
                            </CCol>
                          </CFormGroup>
                        </CCol>

                        <CCol md="6" lg="6" xl="6">
                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">{i18n.language == 'ar' ? `اسم المستخدم` : `User Name`}</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="userUserName"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? `اسم المستخدم` : `User Name`}
                                value={upData.userUserName} />
                            </CCol>
                          </CFormGroup>
                        </CCol>
                        <CCol md="6" lg="6" xl="6">
                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">{i18n.language == 'ar' ? `البريد الالكتروني` : `Email`}</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="email"
                                required
                                type='email'
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? `البريد الالكتروني` : `Email`}
                                value={upData.email} />
                            </CCol>
                          </CFormGroup>
                        </CCol>
                        <CCol md="6" lg="6" xl="6">
                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? `الرقم الداخلي` : `Extention Number`}</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="extentionNumber"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? `الرقم الداخلي` : `Extention Number`}
                                value={upData.extentionNumber} />
                            </CCol>
                          </CFormGroup>
                        </CCol>
                        <CCol md="6" lg="6" xl="6">
                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">{i18n.language == 'ar' ? `كلمة السر` : `Password`}</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="password"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? `كلمة السر` : `Password`}
                                value={upData.password} />
                            </CCol>
                          </CFormGroup>
                        </CCol>
                        <CCol md="6" lg="6" xl="6">
                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? `تأكيد كلمة السر` : `Confirm Password`}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="confirmPassword"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? `تأكيد كلمة السر` : `Confirm Password`}
                                value={upData.confirmPassword} />
                            </CCol>
                          </CFormGroup>
                        </CCol>
                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? `منصب الموظف` : `User Position`}</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CSelect custom name="userPosition" id="select"
                                required value={userPosition} onChange={(e) => handleData(e)}>
                                <option value='' >{i18n.language == 'ar' ? `اختر` : `Select Position`}</option>

                                <option value='chairman'>{i18n.language == 'ar' ? `رئيس` : `Chairman`}</option>
                                <option value='ceo'>{i18n.language == 'ar' ? `مدير تنفيذي` : `CEO`}</option>

                              </CSelect>
                            </CCol>
                          </CFormGroup>

                        </CCol>



                      </CRow>


                    </CCardBody>
                    <CCardFooter className="p-4">
                      <CRow className="justify-content-center">

                        {errorMessage &&
                          <CAlert className='col-lg-12  col-md-12 col-sm-12 col-xs-12 '
                            color="danger"
                            // closeButton
                            show={visible}
                            // closeButton
                            onShowChange={setVisible}
                          >

                            {errorMessage && errorMessage.map((item, i) => (

                              <>{errorMessage[i]}<br /></>

                            ))}
                          </CAlert>}

                        {succesAdd &&

                          <CAlert className='col-lg-12  col-md-12 col-sm-12 col-xs-12 '
                            color="success"
                            show={visible}
                            // closeButton
                            onShowChange={setVisible}
                          // closeButton
                          >
                            {succesAdd}
                          </CAlert>}

                        <CCol md="6" lg="6" xl="6" xs="12" sm="12" >
                          {<CButton color="success" block type='submit'>
                            {t('Save')}
                            {loading && <>{' '}<i className="fa fa-spinner fa-spin" ></i></>} </CButton>}
                        </CCol>

                      </CRow>
                    </CCardFooter>
                  </CCard>



                </CCardBody>

              </CForm>
            </CCol>
          </CRow>

        </CCard>







      </CContainer>
    </div>
  )
}

export default AddNewCompany
