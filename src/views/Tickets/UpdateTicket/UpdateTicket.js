import React, { useState, useEffect } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CPagination,
  CDataTable,
  CSelect,
  CFormText,
  CTextarea,
  CFormGroup,
  CLabel,
  CSwitch,
  CInputFile,
  CLink,
  CFade,
  CCollapse,
  CBadge,
  CRow
} from '@coreui/react'
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import CIcon from '@coreui/icons-react'
import './UpdateTicket.scss'
import TimeField from 'react-simple-timefield';
import { CAlert } from '@coreui/react'
import { useHistory } from "react-router-dom";
import { useTranslation } from 'react-i18next';

import '../../../globalVar'

// import { set } from 'core-js/core/dict'
// import MIN_SAFE_INTEGER from 'core-js/fn/number/min-safe-integer'
const UpdateTicket = ({ location, match }) => {
  const [t, i18n] = useTranslation();
  let history = useHistory();
  const [visible, setVisible] = useState(10)

  const [fetchedData, setfetchedData] = useState([])
  const [refresh, setRefresh] = useState('')
  const [errorMessage, setErrorMessage] = useState();

  const [succesAdd, setSuccessAdd] = useState()
  const [loading, setLoading] = useState('')
  const tokenString = localStorage.getItem("token");
  const userToken = JSON.parse(tokenString);
  const [pickedImg, setPickedImg] = useState('')
  // const userId = localStorage.getItem("user_id");
  // const user_id = JSON.parse(userId);
  const [country, setCountry] = useState('')
  const [city, setCity] = useState('')
  const [priorities, setPriorities] = useState([])
  const [statuses, setStatuses] = useState([])
  const [types, setTypes] = useState([])
  const [departments, setDepartments] = useState([])
  const [upData, setUpData] = useState({
    name: '',
    description: '',
    next_action_date: "",
    customer_id: match.params.id,
    department_id: '',
    ticket_type_id: '',
    ticket_status_id: '',
    ticket_priority_id: '',
    time_to_resolve: ''
  })
  const { name, description, next_action_date, time_to_resolve,

    department_id,
    ticket_type_id,
    ticket_status_id,
    ticket_priority_id,
  } = upData;
  const [phones, setPhones] = useState([""])
  useEffect(async () => {

    const getTicket = async (id) => {

      try {
        const responsee = await fetch(
          `${global.apiUrl}/tickets/${id}`,
          {
            method: "GET",
            headers: {
              Authorization: "Bearer " + userToken,

              Accept: "application/json",
            },
          }
        );
        if (responsee.status == 204) {

        }
        const response = await responsee.json();
        console.log('response', response);
        console.log(response);
        if (response.success == true) {
          setUpData({
            ...upData,
            name: response.payload.name,
            description: response.payload.description,
            next_action_date: response.payload.next_action_date.slice(0, 10),
            department_id: response.payload.department_id,
            ticket_type_id: response.payload.ticket_type_id,
            ticket_status_id: response.payload.ticket_status_id,
            ticket_priority_id: response.payload.ticket_priority_id,
            time_to_resolve: response.payload.next_action_date.slice(11, 16)
          })
        }
      } catch (err) {
        console.log(err);

      }
    }
    const fetchdepts = async (e) => {
      try {
        const responsee = await fetch(
          `${global.apiUrl}/departments?paginate=0`,
          {
            method: "GET",
            headers: {
              Authorization: "Bearer " + userToken,

              Accept: "application/json",
            },
          }
        );
        const response = await responsee.json();
        console.log(response);

        if (response.success) {
          setDepartments(response.payload)
        }


        if (response.message && response.message == "Unauthenticated.") {
          localStorage.removeItem("token");
          localStorage.clear()

          history.push("/login");
        }

      } catch (err) {
        console.log(err);

      }

    }

    const fetchStatuses = async (e) => {
      try {
        const responsee = await fetch(
          `${global.apiUrl}/ticketStatuses?paginate=0`,
          {
            method: "GET",
            headers: {
              Authorization: "Bearer " + userToken,

              Accept: "application/json",
            },
          }
        );
        const response = await responsee.json();
        console.log(response);

        if (response.success) {
          setStatuses(response.payload)
        }


        if (response.message && response.message == "Unauthenticated.") {
          localStorage.removeItem("token");
          localStorage.clear()

          history.push("/login");
        }

      } catch (err) {
        console.log(err);

      }

    }
    const fetchPrioreties = async (e) => {
      try {
        const responsee = await fetch(
          `${global.apiUrl}/ticketPriorities?paginate=0`,
          {
            method: "GET",
            headers: {
              Authorization: "Bearer " + userToken,

              Accept: "application/json",
            },
          }
        );
        const response = await responsee.json();
        console.log(response);

        if (response.success) {
          setPriorities(response.payload)
        }


        if (response.message && response.message == "Unauthenticated.") {
          localStorage.removeItem("token");
          localStorage.clear()

          history.push("/login");
        }

      } catch (err) {
        console.log(err);

      }

    }
    const fetchTypes = async (e) => {
      try {
        const responsee = await fetch(
          `${global.apiUrl}/ticketTypes?paginate=0`,
          {
            method: "GET",
            headers: {
              Authorization: "Bearer " + userToken,

              Accept: "application/json",
            },
          }
        );
        const response = await responsee.json();
        console.log(response);

        if (response.success) {
          setTypes(response.payload)
        }


        if (response.message && response.message == "Unauthenticated.") {
          localStorage.removeItem("token");
          localStorage.clear()

          history.push("/login");
        }

      } catch (err) {
        console.log(err);

      }

    }
    fetchTypes()
    getTicket(match.params.id)
    fetchPrioreties()

    fetchStatuses()

    fetchdepts()
  }, [])




  const handleData = (e) => {
    setUpData({ ...upData, [e.target.name]: e.target.value })

    setErrorMessage('')
    setSuccessAdd('')
  }


  const handlePhones = (i) => (e) => {
    var temp = [...phones]
    temp[i] = e.target.value
    setPhones(
      temp
    );
    // setPhones(
    //   ...[phones, (phones[i] = e.target.value)]
    // );
    console.log('phones', phones)

  };

  // add row
  const [addRow, setaddrow] = useState([]);
  const handladdRow = (e) => {
    setaddrow([...addRow, 1]);
  };



  const handleAdd = async (e) => {
    e.preventDefault()
    setLoading(true)

    setErrorMessage('')
    setSuccessAdd('')

    const data = new FormData();
    name && data.append("name", name);






    try {
      const responsee = await fetch(
        `${global.apiUrl}/tickets/${match.params.id}`,
        {
          method: "PUT",
          headers: {
            Authorization: "Bearer " + userToken,
            "Content-Type": "application/json",
            //'Access-Control-Allow-Origin': 'https://localhost:3000',
            // 'Access-Control-Allow-Credentials': 'true',
            Accept: "application/json",
          },
          body: JSON.stringify({
            name, description, next_action_date: `${next_action_date + ' ' + time_to_resolve.slice(0, 2)}`,
            // customer_id: match.params.id,
            department_id,
            ticket_type_id,
            ticket_status_id,
            ticket_priority_id,
          })
          ,

        }
      );
      const response = await responsee.json();
      console.log('response', response);
      console.log(response);
      setVisible(10)
      if (response.success) {
        await setVisible(6)
        setSuccessAdd(i18n.language == 'ar' ? "تم تعديل بطاقة بنجاح" : "Ticket Updated Successfuly")


        setVisible(6)
      }
      else {

        setVisible(10)
        setErrorMessage(response.messages)


      }


    } catch (err) {
      console.log(err);

    }

    setLoading(false)
  }


  console.log('data', upData)
  return (
    <div className="c-app c-default-layout flex-row align-items-center register-cont">

      <CContainer>


        <CCard className="">



          <CCardHeader>
            <CRow className=" row-gap-15">

              <CCol md="6" lg="6" xl="6" className="justify-content-center align-self-center align-items-center place-items-center text-capitalize">
                <strong> {i18n.language == 'ar' ? "تعديل بطاقة" : "Update Ticket"}</strong>
              </CCol>
              <CCol md="6" lg="6" xl="6" className='row-gap-15 col-gap-15'>

                <CButton color="success" className='col-lg-6  col-md-6 col-sm-12 col-xs-12 updatebtn'
                  onClick={() => history.goBack()} >{i18n.language == 'ar' ? `رجوع` : `Back`}
                </CButton>

              </CCol>
            </CRow>
          </CCardHeader>

          <CRow>
            <CCol xs="12" sm="12" md="12" className=''>
              <CForm onSubmit={(e) => { handleAdd(e) }}>
                <CCardBody>
                  <CCard>
                    <CCardBody>
                      <CRow >
                        <CCol md='12'> <strong>
                          {i18n.language == 'ar' ? "معلومات البطاقة :" : "Ticket Informations :"}
                        </strong></CCol>


                        {/* className="justify-content-center" */}
                        <CCol md="12" lg="12" xl="12">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "اسم البطاقة" : "Ticket Name"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="name"
                                required
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? "اسم البطاقة" : "Ticket Name"}
                                value={upData.name} />
                            </CCol>
                          </CFormGroup>

                        </CCol>


                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "القسم" : "Department"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CSelect custom name="department_id"
                                required value={upData.department_id} onChange={(e) => handleData(e)}>
                                <option value=""  >
                                  {i18n.language == 'ar' ? "اختر  قسم" : "Select Department"}
                                </option>

                                {departments.length > 0 && departments.map((department) => {
                                  return (<option value={department.id} key={department.id}>
                                    {i18n.language == 'ar' ? department.name_ar : department.name_en}
                                  </option>)
                                })}

                              </CSelect>
                            </CCol>
                          </CFormGroup>

                        </CCol>

                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "نوع البطاقة" : "Ticket Type"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CSelect custom name="ticket_type_id"
                                required value={upData.ticket_type_id} onChange={(e) => handleData(e)}>

                                <option value=""  >
                                  {i18n.language == 'ar' ? "اختر  نوع" : "Select Type"}
                                </option>
                                {types.length > 0 && types.map((item) => {
                                  return (<option value={item.id} key={item.id}>
                                    {i18n.language == 'ar' ? item.name_ar : item.name_en}
                                  </option>)
                                })}

                              </CSelect>
                            </CCol>
                          </CFormGroup>

                        </CCol>

                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "حالة البطاقة" : "Ticket Status"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CSelect custom name="ticket_status_id"
                                required value={upData.ticket_status_id} onChange={(e) => handleData(e)}>

                                <option value=""  >
                                  {i18n.language == 'ar' ? "اختر  حالة" : "Select status"}
                                </option>
                                {statuses.length > 0 && statuses.map((item) => {
                                  return (<option value={item.id} key={item.id}>
                                    {i18n.language == 'ar' ? item.name_ar : item.name_en}
                                  </option>)
                                })}

                              </CSelect>
                            </CCol>
                          </CFormGroup>

                        </CCol>

                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "أولوية البطاقة" : "Ticket Priority"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <CSelect custom name="ticket_priority_id"
                                required value={upData.ticket_priority_id} onChange={(e) => handleData(e)}>


                                <option value=""  >
                                  {i18n.language == 'ar' ? "اختر  أولوية" : "Select Priority"}
                                </option>
                                {priorities.length > 0 && priorities.map((item) => {
                                  return (<option value={item.id} key={item.id}>
                                    {i18n.language == 'ar' ? item.name_ar : item.name_en}
                                  </option>)
                                })}

                              </CSelect>
                            </CCol>
                          </CFormGroup>

                        </CCol>
                        <CCol md="6" lg="6" xl="6">

                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">
                                {i18n.language == 'ar' ? "تاريخ الحدث التالي" : "Next Action Date"}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">

                              <CInput name="next_action_date"
                                required
                                type='date'
                                onChange={handleData}
                                placeholder={i18n.language == 'ar' ? "تاريخ الحدث التالي" : "Next Action Date"}
                                value={upData.next_action_date} />
                            </CCol>
                          </CFormGroup>

                        </CCol>

                        <CCol md="3" lg="3" xl="3">
                          <CFormGroup row>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">{i18n.language == 'ar' ? `الساعة` : `Hour`}</CLabel>
                            </CCol>
                            <CCol xs="12" md="12">
                              <TimeField
                                required
                                name="time_to_resolve"
                                className="form-control time_to_resolve "
                                value={upData.time_to_resolve}
                                onChange={handleData}

                              />

                            </CCol>
                          </CFormGroup>
                        </CCol>

                        <CCol md="12" lg="12">
                          <CFormGroup row className=''>
                            <CCol md="12">
                              <CLabel htmlFor="text-input">{i18n.language == 'ar' ? `الوصف` : `Description`}
                              </CLabel>
                            </CCol>
                            <CCol xs="12" md="12">


                              <CTextarea name="description" required rows='3'
                                onChange={handleData}

                                placeholder={i18n.language == 'ar' ? `الوصف` : `Description`}
                                value={upData.description} />


                            </CCol>
                          </CFormGroup>

                        </CCol>

                      </CRow>


                    </CCardBody>
                    <CCardFooter className="p-4">
                      <CRow className="justify-content-center">

                        {errorMessage &&
                          <CAlert className='col-lg-12  col-md-12 col-sm-12 col-xs-12 '
                            color="danger"
                            // closeButton
                            show={visible}
                            // closeButton
                            onShowChange={setVisible}
                          >

                            {errorMessage && errorMessage.map((item, i) => (

                              <>{errorMessage[i]}<br /></>

                            ))}
                          </CAlert>}

                        {succesAdd &&

                          <CAlert className='col-lg-12  col-md-12 col-sm-12 col-xs-12 '
                            color="success"
                            show={visible}
                            // closeButton
                            onShowChange={setVisible}
                          // closeButton
                          >
                            {succesAdd}
                          </CAlert>}

                        <CCol md="6" lg="6" xl="6" xs="12" sm="12" >
                          {<CButton color="success" block type='submit'>
                            {t('Save')}
                            {loading && <>{' '}<i className="fa fa-spinner fa-spin" ></i></>} </CButton>}
                        </CCol>

                      </CRow>
                    </CCardFooter>
                  </CCard>



                </CCardBody>

              </CForm>
            </CCol>
          </CRow>

        </CCard>







      </CContainer>
    </div>
  )
}

export default UpdateTicket 
