import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>

        <a >CRM</a>
        <span className="ml-1">&copy; 2021 CRM.</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by</span>
        <a >IKONIKS</a>

      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
